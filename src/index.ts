//Allow multiple discounts
import {
    calculateBasePrice,
    calculateFinalPrice
} from './actions/priceService'

//Allow single best discount
// import {
//     calculateBasePrice,
//     calculateFinalPrice
// } from './actions/priceServiceMaxDiscount'

import {
    ServiceYear,
    ServiceType,
    ServiceActionType,
    DispatchMessage
} from './types/priceDefinition'

import { SelectionReducer } from './reducers/selectionReducer';

export const updateSelectedServices = (previouslySelectedServices: ServiceType[], action: { type: ServiceActionType; service: ServiceType }) => {
    let reducer = new SelectionReducer(previouslySelectedServices);
    let currentState = reducer.receiveMessage(<DispatchMessage>{
        action: action.type,
        type: action.service
    })

    return currentState.selection;
};

export const calculatePrice = (selectedServices: ServiceType[], selectedYear: ServiceYear) => {
    let basePrice: number = 0;
    let finalPrice: number = 0;

    if (selectedServices.length == 0) {
        return { basePrice, finalPrice }
    }

    basePrice = calculateBasePrice(selectedServices, selectedYear);
    finalPrice = calculateFinalPrice(selectedServices, selectedYear);

    return { basePrice, finalPrice }
};