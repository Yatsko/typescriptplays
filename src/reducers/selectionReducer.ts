import {
    DispatchMessage,
    StateType,
    ServiceType
} from '../types/priceDefinition';

import {
    calculateSelection,
    calculateDeselection
} from '../actions/selectionService'

export class SelectionReducer {
    private state: StateType;

    constructor(prevSelections: ServiceType[] = []) {
        this.state = {
            selection: prevSelections
        }
    }

    public receiveMessage = (message: DispatchMessage): StateType => {
        switch (message.action) {
            case 'Select': {
                if (this.state.selection.indexOf(message.type) == -1) {
                    let selectionResult = calculateSelection(this.state.selection, message.type);
                    this.state = {
                        selection: selectionResult
                    }
                }
                return this.state;
            }

            case 'Deselect': {
                if (this.state.selection.indexOf(message.type) != -1) {
                    let deselectionResult = calculateDeselection(this.state.selection, message.type);
                    this.state = {
                        selection: deselectionResult
                    }
                }
                return this.state;
            }

            default: {
                return this.state;
            }
        }

    }

};