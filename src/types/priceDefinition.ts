export type ServiceYear = 2020 | 2021 | 2022;
export type ServiceType = "Photography" | "VideoRecording" | "BlurayPackage" | "TwoDayEvent" | "WeddingSession";
export type ServiceActionType = "Select" | "Deselect";
export type ServiceAssignmentType = "PriceAssignment" | "DiscountAssignment" | "PackageAssignment" | "ExcludePriceAssignment";

export type ServiceAssignment = {
    serviceYear: ServiceYear;
    serviceType: ServiceType;
    requireType: ServiceType;
    price: number;
    type: ServiceAssignmentType;
}

export type CalculatedAssignmentDiscount = {
    serviceType: ServiceType;
    requireType?: ServiceType;
    price: number;
}

export type DispatchMessage = {
    type: ServiceType;
    action: ServiceActionType;
}

export type StateType = {
    selection: ServiceType[];
}
