import {
    ServiceYear,
    ServiceType,
    ServiceAssignment,
    CalculatedAssignmentDiscount
} from '../types/priceDefinition';

import {
    filterMappings,
    getTypedMapping
} from '../actions/baseMappingService';

function calculateBestPackageAssignments(mappings: Array<ServiceAssignment>): CalculatedAssignmentDiscount[] {
    let discounts = Array<CalculatedAssignmentDiscount>();

    getTypedMapping(mappings, "PackageAssignment").forEach(discountAssignment => {
        let discount = discounts.find(e => e.serviceType == discountAssignment.serviceType && e.requireType == discountAssignment.requireType)

        if (discount) {
            if (discount.price > discountAssignment.price) {
                discount.price = discountAssignment.price;
            }
        } else {
            discounts.push(<CalculatedAssignmentDiscount>{
                serviceType: discountAssignment.serviceType,
                requireType: discountAssignment.requireType,
                price: discountAssignment.price
            });
        }
    });

    return discounts;
}

function calculateBestDiscountAssignments(mappings: Array<ServiceAssignment>): CalculatedAssignmentDiscount[] {
    let discounts = Array<CalculatedAssignmentDiscount>();

    getTypedMapping(mappings, "DiscountAssignment").forEach(discountAssignment => {
        let discount = discounts.find(e => e.serviceType == discountAssignment.serviceType)

        if (discount) {
            if (discount.price > discountAssignment.price) {
                discount.price = discountAssignment.price;
            }
        } else {
            discounts.push(<CalculatedAssignmentDiscount>{
                serviceType: discountAssignment.serviceType,
                price: discountAssignment.price
            });
        }
    });

    return discounts;
}

function calculateDiscountsFromMappings(mappings: Array<ServiceAssignment>): CalculatedAssignmentDiscount[] {
    let bestPackageAssignments = calculateBestPackageAssignments(mappings);
    let bestDiscountAssignments = calculateBestDiscountAssignments(mappings);
    return bestPackageAssignments.concat(bestDiscountAssignments);
}

function calculatePriceFromMappings(selectedServices: ServiceType[], mappings: Array<ServiceAssignment>): number {
    return selectedServices.reduce((a, b) => {
        let assignment = mappings.find(e => e.serviceType == b);
        return a + (assignment ? assignment.price : 0);
    }, 0);
}

function calculateExcludedServices(selectedServices: ServiceType[], mappings: Array<ServiceAssignment>): Array<ServiceType> {
    let existedExclusion = mappings.filter(e => selectedServices.indexOf(e.requireType) != -1 && selectedServices.indexOf(e.serviceType) == -1);
    return selectedServices.filter(e => !existedExclusion.some(f => f.requireType == e));
}

export function calculateFinalPrice(selectedServices: ServiceType[], selectedYear: ServiceYear): number {
    let filteredMappings = filterMappings(selectedServices, selectedYear);

    let excludedAssignments = getTypedMapping(filteredMappings, "ExcludePriceAssignment");
    let excludedSelectedServices = calculateExcludedServices(selectedServices, excludedAssignments);

    let priceCalculation = 0;
    let priceAssignments = getTypedMapping(filteredMappings, "PriceAssignment");

    let discounts = calculateDiscountsFromMappings(filteredMappings.filter((el) => !excludedAssignments.includes(el)));
    discounts.forEach(discount => {
        excludedSelectedServices.splice(excludedSelectedServices.findIndex(p => p === discount.serviceType), 1);
        if(discount.requireType) excludedSelectedServices.splice(excludedSelectedServices.findIndex(p => p === discount.requireType), 1);
        priceCalculation += discount.price;
    });

    priceCalculation += calculatePriceFromMappings(excludedSelectedServices, priceAssignments);

    return priceCalculation;
}

export function calculateBasePrice(selectedServices: ServiceType[], selectedYear: ServiceYear): number {
    let filteredMappings = filterMappings(selectedServices, selectedYear);
    let priceAssignment = getTypedMapping(filteredMappings, "PriceAssignment");
    let priceCalculation = calculatePriceFromMappings(selectedServices, priceAssignment);
    return priceCalculation;
}