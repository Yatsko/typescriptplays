import {
    ServiceYear,
    ServiceType
} from '../types/priceDefinition';

import {
    filterMappingsByYear,
    getTypedMapping
} from '../actions/baseMappingService';

//Because there are lack in the instruction about selection year we choose default
const Year: ServiceYear = 2020;

export function calculateSelection(selectedServices: ServiceType[], newSelection: ServiceType): ServiceType[] {
    let filteredMappings = filterMappingsByYear(Year);
    let excludeAssignment = getTypedMapping(filteredMappings, "ExcludePriceAssignment");

    if (!excludeAssignment.some(e => e.serviceType == newSelection) ||
        excludeAssignment.some(e => e.serviceType == newSelection && selectedServices.indexOf(e.requireType) == 1)) {
        return selectedServices.concat([newSelection]);
    } else {
        return selectedServices;
    }
}

export function calculateDeselection(selectedServices: ServiceType[], newDeselection: ServiceType): ServiceType[] {
    let filteredMappings = filterMappingsByYear(Year);
    let excludeAssignment = getTypedMapping(filteredMappings, "ExcludePriceAssignment");
    let selectedServicesWithDeselection = selectedServices.filter(e => e != newDeselection);
    let selectedServicesExtraExcluded = selectedServicesWithDeselection.filter(e =>
        !excludeAssignment.some(f => f.serviceType == e) ||
        excludeAssignment.some(f => f.serviceType == e && selectedServicesWithDeselection.indexOf(f.requireType) != -1));

    return selectedServicesExtraExcluded;
}