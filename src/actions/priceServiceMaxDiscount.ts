import {
    ServiceYear,
    ServiceType,
    ServiceAssignment,
} from '../types/priceDefinition';

import {
    filterMappings,
    getTypedMapping
} from '../actions/baseMappingService';

function calculateDiscountFromMappings(mappings: Array<ServiceAssignment>): ServiceAssignment {
    let highestDiscount = 0;
    let highestDiscountAssignment = <ServiceAssignment>{};

    let priceAssignments = getTypedMapping(mappings, "PriceAssignment");
    let discountAssignments = getTypedMapping(mappings, "DiscountAssignment");
    let packageAssignments = getTypedMapping(mappings, "PackageAssignment");

    discountAssignments.concat(packageAssignments).forEach(discountAssignment => {
        let discountValue = 0;
        let discountPrice = 0;

        let serviceType = priceAssignments.find(e => e.serviceType == discountAssignment.serviceType);
        let requireType = priceAssignments.find(e => e.serviceType == discountAssignment.requireType);

        if (serviceType && requireType) {
            if (discountAssignment.type == "PackageAssignment") {
                discountValue = serviceType.price + requireType.price - discountAssignment.price;
                discountPrice = discountAssignment.price;
            } else {
                discountValue = serviceType.price - discountAssignment.price;
                discountPrice = requireType.price + discountAssignment.price;
            }

            if (highestDiscount < discountValue) {
                highestDiscount = discountValue;
                highestDiscountAssignment.price = discountPrice;
                highestDiscountAssignment.requireType = discountAssignment.requireType;
                highestDiscountAssignment.serviceType = discountAssignment.serviceType;
            }
        }
    });

    return highestDiscount == 0 ? null : highestDiscountAssignment;
}

function calculatePriceFromMappings(selectedServices: ServiceType[], mappings: Array<ServiceAssignment>): number {
    return selectedServices.reduce((a, b) => {
        let assignment = mappings.find(e => e.serviceType == b);
        return a + (assignment ? assignment.price : 0);
    }, 0);
}

function calculateExcludedServices(selectedServices: ServiceType[], mappings: Array<ServiceAssignment>): Array<ServiceType> {
    let existedExclusion = mappings.filter(e => selectedServices.indexOf(e.requireType) != -1 && selectedServices.indexOf(e.serviceType) == -1);
    return selectedServices.filter(e => !existedExclusion.some(f => f.requireType == e));
}

export function calculateFinalPrice(selectedServices: ServiceType[], selectedYear: ServiceYear): number {
    let filteredMappings = filterMappings(selectedServices, selectedYear);

    let excludedAssignments = getTypedMapping(filteredMappings, "ExcludePriceAssignment");
    let excludedSelectedServices = calculateExcludedServices(selectedServices, excludedAssignments);

    let priceCalculation = 0;
    let priceAssignments = getTypedMapping(filteredMappings, "PriceAssignment");
    let highestDiscount = calculateDiscountFromMappings(filteredMappings.filter((el) => !excludedAssignments.includes(el)));
    if (highestDiscount) {
        excludedSelectedServices.splice(excludedSelectedServices.findIndex(p => p === highestDiscount.requireType), 1);
        excludedSelectedServices.splice(excludedSelectedServices.findIndex(p => p === highestDiscount.serviceType), 1);
        priceCalculation += highestDiscount.price;
    }

    priceCalculation += calculatePriceFromMappings(excludedSelectedServices, priceAssignments);

    return priceCalculation;
}

export function calculateBasePrice(selectedServices: ServiceType[], selectedYear: ServiceYear): number {
    let filteredMappings = filterMappings(selectedServices, selectedYear);
    let priceAssignment = getTypedMapping(filteredMappings, "PriceAssignment");
    let priceCalculation = calculatePriceFromMappings(selectedServices, priceAssignment);
    return priceCalculation;
}