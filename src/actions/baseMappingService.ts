import {
    ServiceYear,
    ServiceType,
    ServiceAssignment,
    ServiceAssignmentType,
} from '../types/priceDefinition';

import {
    Mappings
} from '../types/priceMapping';

export function filterMappings(selectedServices: ServiceType[], selectedYear: ServiceYear): Array<ServiceAssignment> {
    return filterMappingsByYear(selectedYear)
        .filter(e => selectedServices.indexOf(e.serviceType) != -1 && (!e.requireType || selectedServices.indexOf(e.requireType) != -1));
}

export function filterMappingsByYear(selectedYear: ServiceYear): Array<ServiceAssignment> {
    return Mappings.filter(e => e.serviceYear == selectedYear);
}

export function getTypedMapping(mappings: Array<ServiceAssignment>, type: ServiceAssignmentType): Array<ServiceAssignment> {
    return mappings.filter(e => e.type == type);
}